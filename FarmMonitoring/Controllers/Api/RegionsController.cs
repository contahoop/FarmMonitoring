using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using FarmMonitoring.Model;

namespace FarmMonitoring.Web.Controllers.Api
{
    [Produces("application/json")]
    [Route("api/regions")]
    public class RegionsController : BaseRestController<Region>
    {
        public RegionsController(FarmDbContext context): base(context)
        {
        }
    }
}
