using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using FarmMonitoring.Model;

namespace FarmMonitoring.Web.Controllers.Api
{
    [Produces("application/json")]
    [Route("api/farms")]
    public class FarmsController : BaseRestController<Farm>
    {
        public FarmsController(FarmDbContext context): base(context)
        {
        }

        protected override IQueryable<Farm> SetInclude(DbSet<Farm> dbSet)
        {
            return dbSet
                .Include(d => d.Region)
                .Include(d => d.Culture);
        }

        [HttpGet("analytics")]
        public IEnumerable<dynamic> GetAnalytics()
        {
            return DbSet.Select(d => new{
                Culture = d.Culture.Name,
                Region = d.Region.Name,
                Area = d.Area,
                Harvest = d.LastYearHarvestInTonnes });
        }
    }
}
