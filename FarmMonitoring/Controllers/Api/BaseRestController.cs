using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using FarmMonitoring.Model;
using System.Threading;

namespace FarmMonitoring.Web.Controllers.Api
{
    public class BaseRestController<TEntity> : Controller where TEntity : Entity
    {
        protected readonly FarmDbContext Context;
        protected readonly DbSet<TEntity> DbSet;

        public BaseRestController(FarmDbContext context)
        {
            Context = context;
            DbSet = Context.Set<TEntity>();
        }

        protected virtual IQueryable<TEntity> SetInclude(DbSet<TEntity> dbSet)
        {
            return dbSet;
        }

        // GET: api/entitys
        [HttpGet]
        public IEnumerable<TEntity> GetAll()
        {
            return SetInclude(DbSet);
        }

        [HttpGet("page")]
        public async Task<PageResult<TEntity>> GetPage(PagingFilter filter, CancellationToken cancellationToken)
        {
            var items = await SetInclude(DbSet).Skip(filter.Skip).Take(filter.Take).ToListAsync();
            var total = await DbSet.CountAsync(cancellationToken);
            return new PageResult<TEntity>
            {
                Items = items,
                Total = total
            };
        }

        // GET: api/entitys/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetEntity([FromRoute] int id, CancellationToken cancellationToken)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var entity = await SetInclude(DbSet).SingleOrDefaultAsync(m => m.Id == id, cancellationToken);

            if (entity == null)
            {
                return NotFound();
            }

            return Ok(entity);
        }

        // PUT: api/entitys/5
        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateEntity([FromRoute] int id, [FromBody] TEntity entity)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != entity.Id)
            {
                return BadRequest();
            }

            Context.Entry(entity).State = EntityState.Modified;

            try
            {
                await Context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!await EntityExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Entitys
        [HttpPost]
        public async Task<IActionResult> AddEntity([FromBody] TEntity entity)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            DbSet.Add(entity);
            await Context.SaveChangesAsync();

            return CreatedAtAction("GetEntity", new { id = entity.Id }, entity);
        }

        // DELETE: api/Entitys/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteEntity([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var entity = await DbSet.SingleOrDefaultAsync(m => m.Id == id);
            if (entity == null)
            {
                return NotFound();
            }

            DbSet.Remove(entity);
            await Context.SaveChangesAsync();

            return Ok(entity);
        }

        protected async Task<bool> EntityExists(int id)
        {
            return await DbSet.AnyAsync(e => e.Id == id);
        }
    }
}
