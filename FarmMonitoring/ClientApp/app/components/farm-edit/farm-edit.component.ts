import { Component, Input, Output, EventEmitter} from '@angular/core';
import { FarmService } from "../../services/farm.service";
import { IFarm } from "../../interfaces/farm";
import { CultureService } from "../../services/culture.service";
import { RegionService } from "../../services/region.service";
import { IRegion } from "../../interfaces/region";
import { ICulture } from "../../interfaces/culture";

@Component({
    selector: 'farm-edit',
    templateUrl: './farm-edit.component.html',
    styleUrls: ['./farm-edit.component.css']
})
export class FarmEditComponent{

    @Input()
    public farm: IFarm = { id: 0} as IFarm;

    @Input('open')
    set open(value: boolean) {
        if (!this.regions || !this.cultures) {
            //Promise.all(this.regionService.getAll(), this.cultureService.getAll()).
            this.regionService.getAll().then(r => this.regions = r);
            this.cultureService.getAll().then(c => this.cultures = c);
            this.isOpen = value;
        }
        else {
            this.isOpen = value;
        }
    }
    @Output()
    public close = new EventEmitter();

    @Output()
    public updated = new EventEmitter();

    public isOpen: boolean = false;

    public regions: IRegion[];
    public cultures: ICulture[];


    public closeDialog() {
        this.isOpen = false;
        this.close.emit();
    }
    public updatedCloseDialog() {
        this.isOpen = false;
        this.updated.emit();
        this.close.emit();
    }

    public save()
    {
        if (!this.farm.id){
            this.farmService.create(this.farm).then(f => this.updatedCloseDialog());
        }
        else {
            this.farmService.update(this.farm.id, this.farm).then(f => this.updatedCloseDialog());
        }
    }

    constructor(private farmService: FarmService,
        private cultureService: CultureService,
        private regionService: RegionService) {
    }
}
