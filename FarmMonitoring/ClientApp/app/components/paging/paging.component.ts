import { Component, EventEmitter, Input, OnChanges, Output } from '@angular/core';

@Component({
    selector: 'paging',
    templateUrl: './paging.component.html',
    styleUrls: ['./paging.component.css']
})
export class PagingComponent implements OnChanges {

    @Input()
    public page: number;
    @Input()
    public limit: number;
    @Input()
    public total: number;
    @Input()
    public disabled: boolean;

    @Output()
    public changePage = new EventEmitter<{ page: number; limit: number; }>();

    public settings = {
        prevPage: 'Предыдущая',
        nextPage: 'Следующая',
        rangeStep: 5, // количество страниц, на которое смещается активная страница
        rangeStepName: '...' // кнопка +/-N страниц
    };

    public limits = [10, 25, 50, 100];
    public totalPages = 0;
    public pages: string[] = [];

    /**
     * Обновление параметров
     */
    public ngOnChanges(changes:any): void {
        this.page = Number(this.page) || 1; // при загрузке номер страницы приходит в виде строки
        this.totalPages = this.getTotalPages(!this.total);
    }

    /**
     * Количество страниц
     * @param noUpdate Необходимость в обновлении модели
     * @returns {number}
     */
    public getTotalPages(noUpdate: boolean = true): number {
        const update = !noUpdate;
        const result = Math.ceil(this.total / this.limit);
        if (update) {
            this.updateActivePage(result);
        }
        return result;
    }

    /**
     * Оповещение об изменении страницы
     */
    public updateActivePage(page: number): void {
        if (this.page > page && this.page > 1) {
            if (page === 0) {
                page = 1;
            }
            this.page = page;
            this.notify();
        }
    }

    /**
     * Проверка наличия следующей страницы
     */
    public hasNext(): boolean {
        return this.page < this.totalPages;
    }

    /**
     * Проверка наличия предыдущей страницы
     */
    public hasPrevious(): boolean {
        return !!(this.page - 1);
    }

    public refresh(): void {
        this.setPage(1);
    }

    /**
     * Установка страницы
     */
    public setPage(page: string | number, disabled: boolean = false): boolean  {

        if (this.disabled || disabled) false;

        let newPage: number;

        if (page === this.settings.rangeStepName) {
            return false;
        } else {
            newPage = parseInt(page.toString(), 10);
        }
        this.page = newPage;
        this.notify();
        return false;
    }

    /**
     * Обработка кнопки "вперёд"
     */
    public onNext(disabled: boolean): boolean {
        return this.setPage(this.page + 1, disabled);
    }

    /**
     * Обработка кнопки "назад"
     */
    public onPrev(disabled: boolean): boolean {
        return this.setPage(this.page - 1, disabled);
    }

    /**
     * Активная страница?
     */
    public isActive(page: string): boolean {
        return page === this.page.toString();
    }

    /**
     * Получить массив страниц
     */
    public getPages(start: number, end: number): string[] {
        const selectedPage = this.page;
        const first = start;
        const last = end;
        const rangeStep = this.settings.rangeStep;
        const rangeStepName = this.settings.rangeStepName;

        if ((end - start) >= 0) {
            const input = [];
            for (let k = 0; start <= end; start++ , k++) {
                input.push(start.toString());
            }
            let pages: string[] = [];
            // количество страниц меньше определенного количества [1 2 3 4 5]
            // также недопустима ситуация [1 2 3 4 5 ... 6]
            if (input.length <= rangeStep + 1) {
                pages = input;
            } else {
                pages.push(first.toString());
                if (selectedPage === first || selectedPage < first + rangeStep - 1) {
                    // first [(1) (2) (3) (4) 5 ... 10]
                    for (let i = 0; i < rangeStep - 1; i++) {
                        pages.push((i + first + 1).toString());
                    }
                    pages.push(rangeStepName);
                } else if (selectedPage === last || selectedPage > last - rangeStep) {
                    // last [1 ... 6 (7) (8) (9) (10)]
                    pages.push(rangeStepName);
                    for (let i = last - rangeStep; i < last - 1; i++) {
                        pages.push((i + 1).toString());
                    }
                } else {
                    // [1 ... 3 4 (5) 6 7 ... 10]
                    pages.push(rangeStepName);
                    for (let i = (selectedPage - 2); i < (selectedPage + 3); i++) {
                        pages.push(i.toString());
                    }
                    pages.push(rangeStepName);
                }
                pages.push(last.toString());
            }
            this.pages = pages;
        } else {
            this.pages = [];
        }
        return this.pages;
    }

    /**
     * Установка размера страницы
     */
    public setLimit(): void {
        this.setPage(1);
    }

    /**
     * Уведомить "подписанные" компоненты
     */
    private notify(): void {
        this.changePage.emit({ page: this.page, limit: this.limit });
    }

}
