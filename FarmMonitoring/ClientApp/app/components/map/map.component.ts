import { Component, OnInit, Inject, PLATFORM_ID } from '@angular/core';
import { isPlatformBrowser } from '@angular/common';
//import ol from 'openlayers-prebuilt';
import * as $ from 'jquery';
import { FarmService } from "../../services/farm.service";
import { IFarm } from "../../interfaces/farm";
//import 'bootstrap';


@Component({
    selector: 'map',
    templateUrl: './map.component.html',
    styleUrls: ['./map.component.css']
})
export class MapComponent implements OnInit{
    ngOnInit(): void {
        if (!isPlatformBrowser(this.platformId)) return;
        const ol = require('openlayers-prebuilt') as any;
        require('bootstrap');

        /*var iconFeature = new ol.Feature({
            geometry: new ol.geom.Point(ol.proj.fromLonLat([56, 58])),
            name: 'Null Island',
            population: 4000,
            rainfall: 500
        });*/

        var iconStyle = new ol.style.Style({
            image: new ol.style.Icon(({
                anchor: [0.5, 46],
                anchorXUnits: 'fraction',
                anchorYUnits: 'pixels',
                src: 'img/icon.png'
            }))
        });


        var vectorSource = new ol.source.Vector(/*{
            features: [iconFeature]
        }*/);

        var vectorLayer = new ol.layer.Vector({
            source: vectorSource
        });

        //iconFeature.setStyle(iconStyle);

        var map = new ol.Map({
            target: 'map',
            layers: [
                new ol.layer.Tile({
                    source: new ol.source.OSM()
                }),
                vectorLayer
            ],
            view: new ol.View({
                center: ol.proj.fromLonLat([100, 60]),
                zoom: 3
            })
        });

        var popupElement = document.getElementById('popup') as Element;

        var popup = new ol.Overlay({
            element: popupElement,
            positioning: 'bottom-center',
            stopEvent: false,
            offset: [0, -50]
        });
        map.addOverlay(popup);
        var getFarmHtml = this.getFarmHtml;
        // display popup on click
        map.on('click', function (evt: any) {
            var feature = map.forEachFeatureAtPixel(evt.pixel,
                function (feature: any) {
                    return feature;
                });
            if (feature) {
                if ($(popupElement).next('div.popover:visible').length) {
                    var popover = $(popupElement).data('bs.popover');
                    popover.options.content = getFarmHtml(feature.get('data'));
                    popover.setContent();
                }
                var coordinates = (feature.getGeometry() as any).getCoordinates();
                popup.setPosition(coordinates);
                
                $(popupElement).popover({
                    'placement': 'top',
                    'html': true,
                    'content': getFarmHtml(feature.get('data'))
                });
                $(popupElement).popover('show');
            } else {
                //$(popupElement).popover('hide');
                $(popupElement).popover('destroy');
            }
        });

        // change mouse cursor when over marker
        map.on('pointermove', function (e:any) {
            if (e.dragging) {
                $(popupElement).popover('destroy');
                return;
            }
            var pixel = map.getEventPixel(e.originalEvent);
            var hit = map.hasFeatureAtPixel(pixel);
            (map.getTargetElement() as any).style.cursor = hit ? 'pointer' : '';
        });

        this.farmService.getAll().then(data => {
            vectorSource.clear()
            vectorSource.addFeatures(data.filter(f => f.lat && f.lon)
                .map(f => {
                    let feature = new ol.Feature({ geometry: new ol.geom.Point(ol.proj.fromLonLat([f.lon, f.lat])), data: f });
                    feature.setStyle(iconStyle);
                    return feature;
                }));
            map.render();
        });
    }
    public getFarmHtml(farm: IFarm): string{
        return `<div style="width:330px">
                Имя фермера: <b>${farm.farmerName}</b><br>
                Название фермы: <b>${farm.farmName}</b><br>
                Культура: <b>${farm.culture.name}</b><br>
                Регион: <b>${farm.region.name}</b><br>
                Урожай в тоннах за прошлый год: <b>${farm.lastYearHarvestInTonnes}</b><br>
                Площадь фермы в гектарах: <b>${farm.area}</b><br>
                Id: <b>${farm.id}</b></div>`;
    }

    constructor( @Inject(PLATFORM_ID) private platformId: Object, private farmService: FarmService) {
    }
    
}

