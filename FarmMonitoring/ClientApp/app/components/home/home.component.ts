import { Component, OnInit } from '@angular/core';
import { FarmService } from "../../services/farm.service";
import { IFarm } from "../../interfaces/farm";
import { IPageResult } from "../../interfaces/page-result";
import { IPagingFilter } from "../../interfaces/paging-filter";

@Component({
    selector: 'home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit{

    public farms: IFarm[];
    public editModalOpen: boolean = false;
    public currentFarm: IFarm;
    public page: IPageResult<IFarm> = {
        items: [],
        total: 0
    };
    public isLoaded: boolean = false;
    public filter: IPagingFilter = {
        page: 1,
        count: 10
    };

    public onChangePaging({ page, limit }: { page: number, limit: number }): void {
        this.filter.page = page;
        this.filter.count = limit;
        this.load();
    }

    ngOnInit(): void {
        this.load();
    }
    public load(): void {
        this.isLoaded = false;
        this.farmService.getPage(this.filter)
            .then(page => {
                this.page = page;
                this.isLoaded = true;
            });
    }
    public edit(farm: IFarm) {
        this.currentFarm = { ...farm };
        this.editModalOpen = true;
    }

    public add() {
        this.currentFarm = { id: 0 } as IFarm;
        this.editModalOpen = true;
    }

    public delete(farm: IFarm) {
        if (confirm(`Удалить ферму ${farm.farmName}`)) {
            this.farmService.delete(farm.id).then(f => this.load());
        }
    }


    constructor(private farmService: FarmService) {
    }
}
