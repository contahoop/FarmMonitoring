import { Component, OnInit, ElementRef, Inject, PLATFORM_ID } from '@angular/core';
import { isPlatformBrowser } from '@angular/common';
import { FarmService } from "../../services/farm.service";
import { IFarm } from "../../interfaces/farm";

import * as $ from 'jquery';

@Component({
    selector: 'pivot',
    template:'<div id="output"></div>'
})
export class PivotWrapper implements OnInit{

    private farms: IFarm[];


    ngOnInit(): void {
        this.load();
    }
    private load(): void {
        this.farmService.getAanalytics()
            .then(data => this.buildPivot(data));
    }
    private buildPivot(data: any[]): void {
        if (!isPlatformBrowser(this.platformId)) return;
        if (!this.el || 
            !this.el.nativeElement ||
            !this.el.nativeElement.children) {
            console.log('cant build without element');
            return;
        }
        require("jquery-ui-sortable");
        require("pivottable");
        require("pivottable/dist/pivot.ru.js");

        let localeData = data.map(val => {
            return {
                'Культура': val.culture,
                'Регион': val.region,
                'Площадь фермы, га': val.area,
                'Урожай за прошлый год, т': val.harvest
            }
        });
        $("#output", this.el.nativeElement).pivotUI(localeData,
            {
                rows: ['Регион'],
                cols: ['Культура'],
                vals: ['Урожай за прошлый год, т'],
                aggregatorName: "Сумма",
                localeStrings: {apply: "ОК", cancel: "Отмена"}
            }, false, "ru"
        );
    }
    constructor( @Inject(ElementRef) private el: ElementRef,
        @Inject(PLATFORM_ID) private platformId: Object,
        private farmService: FarmService) {
    }
}
