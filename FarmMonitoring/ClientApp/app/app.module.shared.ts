import './rxjs-extensions';

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { HttpModule, } from '@angular/http';
import { RouterModule } from '@angular/router';

import { AppComponent } from './components/app/app.component';
import { NavMenuComponent } from './components/navmenu/navmenu.component';
import { HomeComponent } from './components/home/home.component';
import { MapComponent } from './components/map/map.component';
import { AnalyticsComponent } from './components/analytics/analytics.component';
import { PagingComponent } from './components/paging/paging.component';
import { FarmEditComponent } from "./components/farm-edit/farm-edit.component";
import { PivotWrapper } from "./components/pivot-wrapper/pivot-wrapper.component";

import { CultureService } from './services/culture.service';
import { FarmService } from './services/farm.service';
import { RegionService } from './services/region.service';
import { UrlService } from "./services/url.service";


@NgModule({
    providers: [
        UrlService,
        CultureService,
        RegionService,
        FarmService
    ],
    declarations: [
        AppComponent,
        NavMenuComponent,
        MapComponent,
        HomeComponent,
        AnalyticsComponent,
        FarmEditComponent,
        PagingComponent,
        PivotWrapper
    ],
    imports: [
        CommonModule,
        HttpModule,
        FormsModule,
        RouterModule.forRoot([
            { path: '', redirectTo: 'home', pathMatch: 'full' },
            { path: 'home', component: HomeComponent },
            //{ path: 'counter', component: CounterComponent },
            { path: 'map', component: MapComponent },
            { path: 'analytics', component: AnalyticsComponent },
            //{ path: 'fetch-data', component: FetchDataComponent },
            { path: '**', redirectTo: 'home' }
        ])
    ]
})
export class AppModuleShared {
}
