import { Injectable } from '@angular/core';
import { Http } from '@angular/http';

import { EditService } from './edit.service';
import { ReadService } from './read.service';

import { ICulture } from '../interfaces/culture.d';
import { IPagingFilter } from '../interfaces/paging-filter.d';
import { UrlService } from "./url.service";

@Injectable()
export class CultureService extends ReadService<ICulture, IPagingFilter> {

    public url = 'api/cultures';

    constructor(public http: Http, urlService: UrlService) {
        super(http, urlService);
    }
}
