import { Http, Response, URLSearchParams } from '@angular/http';
import { IPageResult } from '../interfaces/page-result';
import { ReadService } from './read.service';
import { UrlService } from "./url.service";

export class EditService<TEntity, TFilter> extends ReadService<TEntity, TFilter> {

    constructor(http: Http, urlService: UrlService) {
        super(http, urlService);
    }

    public create(item: TEntity): Promise<TEntity> {
        return this.http.post(this.urlService.toAbsolute(this.url), item)
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }

    public update(id: number | string, item: TEntity): Promise<TEntity> {
        return this.http.put(this.urlService.toAbsolute(`${this.url}/${id}`), item)
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }

    public delete(id: number | string): Promise<any> {
        return this.http.delete(this.urlService.toAbsolute(`${this.url}/${id}`))
            .toPromise()
            .catch(this.handleError);
    }
}
