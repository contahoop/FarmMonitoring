import { Injectable } from '@angular/core';
import { Http } from '@angular/http';

import { EditService } from './edit.service';
import { ReadService } from './read.service';

import { IFarm } from '../interfaces/farm.d';
import { IPagingFilter } from '../interfaces/paging-filter.d';
import { UrlService } from "./url.service";

@Injectable()
export class FarmService extends EditService<IFarm, IPagingFilter> {

    public url = 'api/farms';

    constructor(public http: Http, urlService: UrlService) {
        super(http, urlService);
    }

    public getAanalytics(): Promise<any[]> {
        return this.http.get(this.urlService.toAbsolute(`${this.url}/analytics`))
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }
    
}
