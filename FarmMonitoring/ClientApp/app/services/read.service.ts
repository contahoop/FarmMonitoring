import { Http, Response, URLSearchParams } from '@angular/http';

import { IPageResult } from '../interfaces/page-result';
import { UrlService } from "./url.service";

export class ReadService<TEntity, TFilter> {

    public url: string;

    constructor(public http: Http, public urlService: UrlService) {
    }

    public get(id: number | string): Promise<TEntity> {
        return this.http.get(this.urlService.toAbsolute(`${this.url}/${id}`))
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }

    public getAll(): Promise<TEntity[]> {
        return this.http.get(this.urlService.toAbsolute(this.url))
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }

    public getPage(filter: TFilter): Promise<IPageResult<TEntity>> {
        return this.http.get(this.urlService.toAbsolute(`${this.url}/page`), { search: filter })
            .toPromise()
            .then((this.extractData))
            .catch(this.handleError);
    }

    public extractData(res: Response): any {
        let body = {} as any;
        try {
            body = res.json();
        } catch (e) {
            // post return null
        }
        return body;
    }

    public handleError(errorResponse: Response | any): Promise<any> {
        if (errorResponse instanceof Response) {
            const body = errorResponse.json() || '';
            const errorMessage = body.message || body.error || JSON.stringify(body, null, 2);
            console.error(errorMessage);
        }
        return Promise.reject(errorResponse);
    }
}
