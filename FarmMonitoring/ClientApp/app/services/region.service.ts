import { Injectable } from '@angular/core';
import { Http } from '@angular/http';

import { EditService } from './edit.service';
import { ReadService } from './read.service';

import { IRegion } from '../interfaces/region.d';
import { IPagingFilter } from '../interfaces/paging-filter.d';
import { UrlService } from "./url.service";

@Injectable()
export class RegionService extends ReadService<IRegion, IPagingFilter> {

    public url = 'api/regions';

    constructor(public http: Http, urlService: UrlService) {
        super(http, urlService);
    }
}
