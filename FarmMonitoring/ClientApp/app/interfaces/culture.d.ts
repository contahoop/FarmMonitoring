// Культура
export interface ICulture {
		/** Идентификатор */
		id: number;
		/** Наименование */
		name: string;
	}
