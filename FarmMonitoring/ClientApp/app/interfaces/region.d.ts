// Регион
export interface IRegion {
	/** Идентификатор */
	id: number;
	/** Наименование */
	name: string;
}
