export interface IPagingFilter {
    page: number;
    count: number;
}
