import { ICulture } from './culture.d'
import { IRegion } from './region.d'

export interface IFarm {
	/** Идентификатор */
	id: number;
	/** Имя фермера */
	farmerName: string;
	/** Название фермы */
	farmName: string;
	/** Идентификатор культуры */
	cultureId: number;
	/** Культура */
    culture: ICulture;
	/** Идентификатор региона */
	regionId: number;
	/** Регион */
    region: IRegion;
	/** Урожай в тоннах за прошлый год */
	lastYearHarvestInTonnes: number;
	/** Площадь фермы в гектарах */
    area: number;
    /**Широта*/
    lat: number;
    /**Долгота*/
    lon: number;
}
