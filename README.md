# Приложение для ввода и анализа данных по фермам

## Установка

Потребуются Visual Studio 2017 и Node.js,
sql сервер - Microsoft Sql Server 2017 (хотя возможно будет работать и на более ранних)
далее необходимо проверить строку подключения в \FarmMonitoring\appsettings.json

Запуск - достаточно нажать F5 в студии, БД должна накатиться автоматически


## Работа с миграциями

Все операции проводить в окне Package Manager Console с выбранным проектом FarmMonitoring.Model

### Обновить БД: 
``` 
Update-Database
 ```

### Откатить БД до миграции: 
``` 
Update-Database –TargetMigration [name]
```

### Добавить миграцию: 
``` 
Add-Migration [name]
```