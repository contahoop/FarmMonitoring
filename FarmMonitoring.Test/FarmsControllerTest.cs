using FarmMonitoring.Web.Controllers.Api;
using System.Linq;
using Xunit;

namespace FarmMonitoring.Test
{
    public class FarmsControllerTest
    {
        [Fact]
        public async void InsertTest()
        {
            var dbContext = DbContextHelper.GetContext("FarmsControllerTest");
            dbContext.Regions.Add(new Model.Region { Id = 1, Name = "Тестовый регион" });
            dbContext.Cultures.Add(new Model.Culture { Id = 1, Name = "Тестовая культура" });
            dbContext.SaveChanges();

            var controller = new FarmsController(dbContext);

            var data = controller.GetAll();
            Assert.Equal(data.Count(), 0);

            await controller.AddEntity(new Model.Farm
            {
                FarmName = "тест",
                FarmerName = "тест",
                CultureId = 1,
                RegionId = 1,
                LastYearHarvestInTonnes = 10,
                Area = 10
            });

            data = controller.GetAll();
            Assert.Equal(data.Count(), 1);
        }
    }
}
