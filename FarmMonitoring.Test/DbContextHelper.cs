using FarmMonitoring.Model;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace FarmMonitoring.Test
{
    public static class DbContextHelper
    {
        public static FarmDbContext GetContext(string dbName = null)
        {
            if (string.IsNullOrEmpty(dbName))
            {
                dbName = Guid.NewGuid().ToString();
            }
            var options = new DbContextOptionsBuilder<FarmDbContext>()
                .UseInMemoryDatabase(databaseName: dbName)
                .Options;

            return new FarmDbContext(options);
        }
    }
}
