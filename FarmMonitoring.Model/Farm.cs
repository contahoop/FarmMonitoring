using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace FarmMonitoring.Model
{
    /// <summary>
    /// Ферма
    /// </summary>
    public class Farm : Entity
    {
        /// <summary>
        /// Имя фермера
        /// </summary>
        [MaxLength(1000)]
        public string FarmerName { get; set; }

        /// <summary>
        /// Название фермы
        /// </summary>
        [MaxLength(1000)]
        public string FarmName { get; set; }

        /// <summary>
        /// Идентификатор культуры
        /// </summary>
        public int CultureId { get; set; }

        /// <summary>
        /// Культура
        /// </summary>
        public Culture Culture { get; set; }

        /// <summary>
        /// Идентификатор региона
        /// </summary>
        public int RegionId { get; set; }

        /// <summary>
        /// Регион
        /// </summary>
        public Region Region { get; set; }

        /// <summary>
        /// Урожай в тоннах за прошлый год
        /// </summary>
        public double LastYearHarvestInTonnes { get; set; }

        /// <summary>
        /// Площадь фермы в гектарах
        /// </summary>
        public double Area { get; set; }

        /// <summary>
        /// Широта
        /// </summary>
        public double? Lat { get; set; }

        /// <summary>
        /// Долгота
        /// </summary>
        public double? Lon { get; set; }
    }
}
