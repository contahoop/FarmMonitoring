using static System.Math;

namespace FarmMonitoring.Model
{
    /// <summary>
    /// Пэйджинг
    /// </summary>
    public class PagingFilter
    {
        public PagingFilter()
        {
            MaxCount = 100;
            Count = 100;
            Page = 1;
        }

        private int _count;
        private int _page;

        /// <summary>
        /// Максимально количество записей на странице
        /// </summary>
        protected int MaxCount;

        /// <summary>
        /// Всего страниц
        /// </summary>
        public decimal Total { get; set; }

        /// <summary>
        /// Количество записей на странице
        /// </summary>
        public int Count
        {
            get { return _count; }
            set
            {
                _count = value < 1 ? 1 : (value > MaxCount ? MaxCount : value);
            }
        }

        /// <summary>
        /// Страница
        /// </summary>
        public int Page
        {
            get { return _page; }
            set
            {
                _page = value <= 0 ? 1 : value;
            }
        }

        /// <summary>
        /// Страниц
        /// </summary>
        public int Pages => (int)Ceiling(Total / Count);

        /// <summary>
        /// Индекс страници
        /// </summary>
        public int Index => Page - 1;

        /// <summary>
        /// Сколько элементов пропустить
        /// </summary>
        public int Skip => Index * Count;

        /// <summary>
        /// Сколько элементов получить
        /// </summary>
        public int Take => Count;
    }
}
