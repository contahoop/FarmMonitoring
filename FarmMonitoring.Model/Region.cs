using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace FarmMonitoring.Model
{
    /// <summary>
    /// Регион
    /// </summary>
    public class Region : Entity
    {
        /// <summary>
        /// Наименование
        /// </summary>
        [MaxLength(200)]
        public string Name { get; set; }
    }
}
