using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace FarmMonitoring.Model
{
    /// <summary>
    /// Культура
    /// </summary>
    public class Culture: Entity
    {
        /// <summary>
        /// Наименование
        /// </summary>
        [MaxLength(200)]
        public string Name { get; set; }
    }
}
