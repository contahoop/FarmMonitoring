using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace FarmMonitoring.Model.Migrations
{
    public partial class lonlat : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<double>(
                name: "Lat",
                table: "Farms",
                type: "float",
                nullable: true);

            migrationBuilder.AddColumn<double>(
                name: "Lon",
                table: "Farms",
                type: "float",
                nullable: true);
            migrationBuilder.Sql("UPDATE [dbo].[Farms] SET [Lat] = Round(RAND(CHECKSUM(NewId()))*23+44,2), [Lon]= Round(RAND(CHECKSUM(NewId()))*135+30,2)");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Lat",
                table: "Farms");

            migrationBuilder.DropColumn(
                name: "Lon",
                table: "Farms");
        }
    }
}
