using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace FarmMonitoring.Model
{
    /// <summary>
    /// Базовый класс для энтити
    /// </summary>
    public abstract class Entity
    {
        /// <summary>
        /// Идентификатор
        /// </summary>
        [Key]
        public int Id { get; set; }
    }
}
